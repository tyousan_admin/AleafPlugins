# AleafPlugins

#### 介绍
leaf的iOS配套扩展插件(含用友和DCloud平台配套插件使用说明)

## 重要 

苹果上架遇见4.3问题，可能需要你自己制作appex扩展文件，防止上架审核4.3问题。

1.用xcode创建一个App工程。

2.添加target，选择Network Extension。

3.导入ALeafKit.framework。

4.修改PacketTunnelProvider.swift内容为。

```js
import NetworkExtension
import ALeafKit

class PacketTunnelProvider: ALeafTunnelProvider {

    override init() {
        super.init()
    }

    override func startTunnel(options: [String : NSObject]?, completionHandler: @escaping (Error?) -> Void) {
        super.startTunnel(options: options, completionHandler: completionHandler)
    }

    override func stopTunnel(with reason: NEProviderStopReason, completionHandler: @escaping () -> Void) {
        super.stopTunnel(with: reason, completionHandler: completionHandler)
    }

    override func handleAppMessage(_ messageData: Data, completionHandler: ((Data?) -> Void)?) {
        super.handleAppMessage(messageData, completionHandler: completionHandler)
    }

    override func sleep(completionHandler: @escaping () -> Void) {
        super.sleep(completionHandler: completionHandler)
    }

    override func wake() {
        super.wake()
    }
}
```

5.运行程序，获取PacketTunnel.appex。

6.删除PacketTunnel.appex内部的embedded.mobileprovision文件。

7.将PacketTunnel.appex内部的Frameworks目录下的ALeafKit.framework复制到XF-AleafPlugins/ios/目录下。

8.然后删除PacketTunnel.appex内部的Frameworks目录。

9.删除PacketTunnel.appex内部的Info.plist中Bundle identifier。

10.将ALeafKit.framework内部的cacert.pem文件copy到PacketTunnel.appex内部

11.替换XF-AleafPlugins里面的PacketTunnel.appex文件。

12.你自己的appex扩展就做好了。

## ** 一、用友 AleafPlugins配置说明 ** 

#### 主工程 Info.plist 需要配置

1. Info.plist中新增groupName值：如group.主工程ID。

```js
<key>groupName</key>
<string>group.主工程ID</string>
<key>NSSystemExtensionUsageDescription</key>
<string>VPN权限申请描述信息</string>
```

#### 主工程 UZApp.entitlements 需要配置

```js
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>com.apple.developer.networking.networkextension</key>
	<array>
		<string>packet-tunnel-provider</string>
	</array>
	<key>com.apple.security.application-groups</key>
	<array>
		<string>group.com.soft.qingyanjiasu</string>
	</array>
</dict>
</plist>
```

#### 扩展库 PacketTunnel.appex 

1. appex扩展内部 Info.plist 根据实际情况填写一下内容。
2. Info.plist中新增Bundle identifier值：如:主工程ID.PacketTunnel。
3. Info.plist中新增groupName值：如group.主工程ID。
4. 根据自己实际情况替换appex中的embedded.mobileprovision证书

```js
<key>CFBundleIdentifier</key>
<string>主工程ID.PacketTunnel</string>
<key>groupName</key>
<string>group.主工程ID</string>
```

## ** 二、Dcloud XF-AleafPlugins配置说明 ** 

1. 修改ios-extension.json里面配置信息：

```js
{
	"PacketTunnel.appex": {
		"identifier":"",//必须修改为自己的扩展id值
		"profile":"ios-PacketTunnelExt.mobileprovision",//可以不修改
		"plists":{
			"groupName":"",//App Group数据共享ID
		},
		"entitlements":{
			"com.apple.developer.networking.networkextension":["packet-tunnel-provider"],
			"com.apple.security.application-groups":[""]//App Group数据共享ID
		}
	}
}
```

2. 替换ios-PacketTunnelExt.mobileprovision描述文件为你自己的扩展描述文件(文件名称请不要变).

3. 修改package.json里面com.apple.security.application-groups信息为自己的值，同时修改NSSystemExtensionUsageDescription权限的描述信息。

![](https://gitee.com/tyousan_admin/AleafPlugins/raw/master/1692797571669.png)


## ** 三、离线打包 ** 

#### 主工程和扩展工程配置如图

![](https://gitee.com/tyousan_admin/AleafPlugins/raw/master/1692797571666.jpg)

#### 主工程单独引入扩展 PacketTunnel.appex 如图

![](https://gitee.com/tyousan_admin/AleafPlugins/raw/master/1692797571667.png)

![](https://gitee.com/tyousan_admin/AleafPlugins/raw/master/1692797571668.png)


同时在Info.Plist中填写权限NSSystemExtensionUsageDescription的描述信息。
